terraform {
    required_providers {
        gitlab = {
            source = "gitlabhq/gitlab"
            version = "3.7.0"
        }
    }
}

provider "gitlab" {
    base_url =   "https://gitlab.com/"
    token =      var.gitlab_token
    insecure =   true
}

resource "gitlab_project_membership" "gitlab-tf-provider-example" {
    project_id = "33597609"
    user_id = "7611428" # @brad
    access_level = "maintainer"
}