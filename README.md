# GitLab Terraform Provider Example

This project makes use of the [GitLab Terraform Provider](https://registry.terraform.io/providers/gitlabhq/gitlab/latest). 

### What this is:

Just an example of how to use the GitLab Terraform Provider. It reads this project's membership state and adds users via resource definition. 

### What this is not:

This is **not** an example of production-ready provider usage. This is only meant to serve as an example. 

### Project Breakdown

In order to successfully manage your project's state in CI/CD, you will need to create a [personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) with the `api` scope in order to access the [Users API endpoint](https://docs.gitlab.com/ee/api/users.html). `$CI_JOB_TOKEN` is limited to specific API endpoints and won't work for this projects usage ([see here](https://docs.gitlab.com/ee/ci/jobs/ci_job_token.html)).

`main.tf` instructs Terraform to install the `gitlabhq/gitlab` provider, configures the provider's metadata, and manages the `gitlab_project_membership`.

`variables.tf` is required in order to use `var.gitlab_token` in `main.tf` to authenticate to the GitLab API. For this example, `TF_VAR_gitlab_token="$GITLAB_TOKEN"` is appended prior to `gitlab-terraform apply` and is authenticating via a custom CI/CD variable named `GITLAB_TOKEN`, which is [masked](https://docs.gitlab.com/ee/ci/variables/#mask-a-cicd-variable). 
