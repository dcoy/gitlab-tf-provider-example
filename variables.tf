variable "gitlab_token" {
    type = string
    description = "Token to authenticate with GitLab API"
    default = "noTokenProvided"
    sensitive = true
}